#Para api
import requests 
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from django.core.cache import cache
import json

def requests_retry_session(
    retries=3,
    backoff_factor=0.3,
    status_forcelist=(500, 502, 504),
    session=None,
):
    session = session or requests.Session()
    retry = Retry(
        total=retries,
        read=retries,
        connect=retries,
        backoff_factor=backoff_factor,
        status_forcelist=status_forcelist,
    )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session

#Funciones API
def obtener_organismos():
    organismos = cache.get("organismos")
    if organismos is None:
        r = requests_retry_session().get('http://organigrama.jujuy.gob.ar/ws_org/')
        orgs = json.loads(r.text)['data']
        organismos = list()
        for org in orgs:
            organismos.append((org['id'],org['nombre']))
        cache.set("organismos", organismos)
    return organismos
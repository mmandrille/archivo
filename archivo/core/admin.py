from django.contrib import admin
#Import modelos propios
from .models import Archivo

#Modificamos nuestros modelos aca:
class ArchivoAdmin(admin.ModelAdmin):
    model = Archivo
    search_fields = ['nombre', 'expediente']
    list_filter = ['organismo', ]

# Register your models here.
admin.site.register(Archivo, ArchivoAdmin)
